﻿using System;
using System.Threading.Tasks;
using WCFClientPCL;

namespace ConsoleAppToTestFault
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }

        static async Task MainAsync(string[] args)
        {
            var ipAddress = "localhost";

            Console.WriteLine("#### PLEASE HAVE SIMPLE FAULT GENERATING SERVER RUNNING Before Testing this ###");
            Console.WriteLine("#### You can either start the SimpleFaultGeneratingServer project without debugging OR open the solution in a separate instance of Visual Studio and start the project in debug mode.  ###");

            Console.WriteLine("Calling Simple Server to generate Fault. No FaultActor element is provided. This Works in both IOS and Regular Cosnole Application.");
            Console.WriteLine("The Response XML looks as follows:");
            Console.WriteLine(SimpleHTTPServer.NoFaultActorInput);

            try
            {
                string s = await ProxyWrapper.TestGenerateFaultTaskBased($"http://{ipAddress}:6968/NoFaultActorEndPoint", "a", "b");
                Console.WriteLine();
            }
            catch (CustomUnWrappedException ex)
            {
                Console.WriteLine("We Caught The Exception We were Looking For!:");
                Console.WriteLine(ex.Message);
                Console.WriteLine();
            }
            catch (Exception ex2)
            {
                Console.WriteLine("Did not Catch Exception We are looking to catch. Please make sure SimpleFaultGeneratingServer is running: \r\n" + ex2.Message);
            }

            Console.WriteLine("Press Enter to continue testing to sending exact message with out <FaultActor> Element Present");
            Console.ReadLine();

            Console.WriteLine("Calling Simple Server to generate Fault. FaultActor element is provided. This Works in both IOS and Regular Cosnole Application.");
            Console.WriteLine("The Response XML looks as follows:");

            try
            {
                string s = await ProxyWrapper.TestGenerateFaultTaskBased($"http://{ipAddress}:6968/FaultActorEndPoint", "a", "b");
                Console.WriteLine();
            }
            catch (CustomUnWrappedException ex)
            {
                Console.WriteLine("We Caught The Exception We were Looking For!:");
                Console.WriteLine(ex.Message);
                Console.WriteLine();
            }
            catch (Exception ex2)
            {
                Console.WriteLine("Did not Catch Exception We are looking to catch. Please make sure SimpleFaultGeneratingServer is running: \r\n" + ex2.Message);
            }

            Console.WriteLine("Notice How Both Custom Fault Exeptions where caught?. This is a regular console project referencing the PCL");
            Console.WriteLine("If we try the same thing in IOS application, only the message without the FaultActor element being present is caught correctly.");

            Console.ReadLine();

        }
    }
}
