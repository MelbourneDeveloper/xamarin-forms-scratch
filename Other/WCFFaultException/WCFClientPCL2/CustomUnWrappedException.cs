﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCFClientPCL.FaultService;

namespace WCFClientPCL
{
    public class CustomUnWrappedException :Exception
    {
        private CustomFault CustomFault;

        public CustomUnWrappedException():
            base()
        {

        }

        public CustomUnWrappedException(CustomFault Cust) : base()
        {
            this.CustomFault = Cust;
        }

        public override string Message
        {
            get
            {
                return CustomFault.CustomErrorInfo + " + ErrorCode: " + CustomFault.ErrorCode;
            }
        }

    }
}
