﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WCFClientPCL.FaultService;

namespace WCFClientPCL
{
    public class ProxyWrapper
    {

        public static async Task<string> TestGenerateFaultTaskBased(string endpoint, string a, string b)
        {
            try
            {
                FaultService.FaultGeneratingServiceClient client = new FaultGeneratingServiceClient(FaultGeneratingServiceClient.EndpointConfiguration.BasicHttpBinding_IFaultGeneratingService, endpoint);
                
                var t = Task<string>.Factory.FromAsync<string, string>(
                    ((IFaultGeneratingService)client.InnerChannel).BeginFaultGeneratingMethod,
                    ((IFaultGeneratingService)client.InnerChannel).EndFaultGeneratingMethod, a, b, null);
                string s = await t;

                return s;
            }
            catch (FaultException<CustomFault> CustomServiceFault)
            {
                throw new CustomUnWrappedException(CustomServiceFault.Detail);
            }
            catch(Exception ex)
            {
                throw new Exception("Did Not Catch CustomFault. Something Wrong: " + ex.Message, ex);
            }
        }
    }
}
