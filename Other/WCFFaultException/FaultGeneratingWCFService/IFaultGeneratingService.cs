﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace FaultGeneratingWCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract(Namespace ="http://RamiCorpSolutions/Services")]
    public interface IFaultGeneratingService
    {

        [OperationContract]
        [FaultContract(typeof(CustomFault), Namespace = "http://RamiCorpSolutions/BusinessObjects")]
        string FaultGeneratingMethod(string a, string b);
        // TODO: Add your service operations here
    }


    public enum ErrorCode
    {
        ERROR1,
        ERROR2,
        ERROR3
    }


    [DataContract(Namespace ="http://RamiCorpSolutions/BusinessObjects")]
    public class CustomFault
    {
        [DataMember]
        public ErrorCode ErrorCode { get; set; }

        [DataMember]
        public string CustomErrorInfo {get;set;}
    }

    
}
