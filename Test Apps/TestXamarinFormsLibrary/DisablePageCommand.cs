using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace TestXamarinFormsLibrary
{
    public class DisablePageCommand : ICommand
    {
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            Application.Current.MainPage.IsEnabled = false;
        }

        public event EventHandler CanExecuteChanged;
    }
}