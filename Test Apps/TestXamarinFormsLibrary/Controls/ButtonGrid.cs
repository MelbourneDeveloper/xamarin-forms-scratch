﻿using System;
using Xamarin.Forms;

namespace TestXamarinForms
{
    public class ButtonGrid : Grid
    {
        #region Bindable Properties
        public static readonly BindableProperty ContentProperty =
            BindableProperty.Create(
                nameof(Content),
                typeof(View),
                typeof(ButtonGrid),
                null,
                propertyChanging: (bindableObject, oldValue, newValue) =>
                {
                    ((ButtonGrid)bindableObject).ContentChanging();
                },
                propertyChanged: (bindableObject, oldValue, newValue) =>
                {
                    ((ButtonGrid)bindableObject).ContentChanged();
                }
            );

        private void ContentChanged()
        {
            if(Children.Count==2)
            {
                Children.RemoveAt(0);
            }

            Children.Add(Content);
        }

        private void ContentChanging()
        {
        }

        #endregion

        #region Events
        public event EventHandler SaveClicked;
        public event EventHandler DeleteClicked;
        #endregion

        #region Public Properties
        public View Content
        {
            get
            {
                return (View)GetValue(ContentProperty);
            }
            set
            {
                SetValue(ContentProperty, value);
            }
        }
        #endregion

        #region Constructor
        public ButtonGrid()
        {
            RowDefinitions.Add(new RowDefinition { });
            RowDefinitions.Add(new RowDefinition { Height = new GridLength(50) });

            var buttonPanel = new ButtonPanel();
            buttonPanel.SaveClicked += SaveClicked;
            buttonPanel.DeleteClicked += DeleteClicked;

            Children.Add(buttonPanel);

            SetRow(buttonPanel, 1);
        }
        #endregion
    }
}
