﻿using Xamarin.Forms;

namespace TestXamarinForms.AsyncListView
{
    public partial class ItemModelProvider
    {
        private void TimerCallback(object state)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                AddItems();
            });
        }
    }
}
