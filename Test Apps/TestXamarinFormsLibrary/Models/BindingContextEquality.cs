﻿using System.ComponentModel;
using System.Diagnostics;

namespace Models
{
    public class BindingContextEqualityModel : INotifyPropertyChanged
    {
        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Fields
        private string _Test;
        private int _Key;
        #endregion

        #region Public Properties
        public string Test
        {
            get => _Test;
            set
            {
                _Test = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Test)));
            }
        }

        public int Key
        {
            get => _Key;
            set
            {
                _Key = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Key)));
            }
        }
        #endregion

        #region Public Overrides
        public override bool Equals(object obj)
        {
            //Why is this even hit?
            Debug.WriteLine("Why is this getting hit? The BindableObject shouldn't care about the equality of a new object with the old object");

            var bindingContextEqualityModel = obj as BindingContextEqualityModel;
            if (bindingContextEqualityModel != null)
            {
                if (bindingContextEqualityModel.Key == Key)
                {
                    return true;
                }
            }
            return false;
        }
        #endregion
    }
}
