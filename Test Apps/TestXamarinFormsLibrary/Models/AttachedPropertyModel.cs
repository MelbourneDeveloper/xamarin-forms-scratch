﻿using System.ComponentModel;

namespace TestXamarinForms.Models
{
    public class AttachedPropertyModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string _Text;

        public string Text
        {
            get
            {
                return _Text;
            }
            set
            {
                _Text = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Text)));
            }
        }
    }
}
