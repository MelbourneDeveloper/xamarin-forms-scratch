﻿
using Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BindingContextEqualityPage : ContentPage
    {
        public BindingContextEqualityPage()
        {
            InitializeComponent();
            BindingContextChanged += BindingContextEqualityPage_BindingContextChanged;
            BindingContext = new BindingContextEqualityModel { Key = 1, Test = "Old Model" };
        }

        private void BindingContextEqualityPage_BindingContextChanged(object sender, System.EventArgs e)
        {
            var bindingContext = (BindingContextEqualityModel)BindingContext;
            DisplayAlert("changed", $"Binding Context Changed. Test: {bindingContext.Test}", "OK");
        }

        private void ShowBug_Clicked(object sender, System.EventArgs e)
        {
            //Notice the Test property has changed, but the binding engine doesn't care because it thinks the object is the same
            BindingContext = new BindingContextEqualityModel { Key = 1, Test = "New Model" };
        }

        private void ShowOK_Clicked(object sender, System.EventArgs e)
        {
            //Notice the Test property has changed, but the binding engine doesn't care
            BindingContext = new BindingContextEqualityModel { Key = 2, Test = "New Model" };
        }
    }
}