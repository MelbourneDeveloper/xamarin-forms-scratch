﻿
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestXamarinForms
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DateDisplayPage : ContentPage
    {
        public DateDisplayPage()
        {
            InitializeComponent();

            var date = new DateTime(2000, 1, 31);

            ToStringBox.Text = date.ToString();
            BindingContext = new TestModel { TheDate = date };
        }
    }

    public class TestModel
    {
        public DateTime TheDate { get; set; }
    }

}