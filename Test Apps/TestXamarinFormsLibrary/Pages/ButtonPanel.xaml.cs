﻿using System;

namespace TestXamarinForms
{
    public partial class ButtonPanel
    {
        #region Events
        public event EventHandler SaveClicked;
        public event EventHandler DeleteClicked;
        #endregion

        #region Constructor
        public ButtonPanel()
        {
            InitializeComponent();
            SaveButton.Clicked += SaveClicked;
            DeleteButton.Clicked += DeleteClicked;
        }
        #endregion
    }
}
