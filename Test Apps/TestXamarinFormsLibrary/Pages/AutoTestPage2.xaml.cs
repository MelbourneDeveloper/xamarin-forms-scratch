﻿using TestXamarinFormsLibrary.Models;
using Xamarin.Forms.Xaml;

namespace TestXamarinFormsLibrary.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AutoTestPage2
    {
        public AutoTestPage2()
        {
            InitializeComponent();
            ChangeHeightButton.Clicked += ChangeSizeButton_Clicked;

            var items = new FruitList
            {
                new Fruit { Name = "test" },
                new Fruit { Name = "test2" },
                new Fruit { Name = "test3" },
                new Fruit { Name = "test4" },
                new Fruit { Name = "test5" }
            };

            OptionsList.ItemsSource = items;
        }

        private void ChangeSizeButton_Clicked(object sender, System.EventArgs e)
        {
            FilterEntry.HeightRequest = FilterEntry.HeightRequest == 300 ? 0 : 300;
        }
    }
}