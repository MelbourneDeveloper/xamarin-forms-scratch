﻿
using Xamarin.Forms;

namespace TestXamarinForms
{
    public partial class InvisibleTabbedPage 
    {
        public InvisibleTabbedPage()
        {
            InitializeComponent();

            foreach (ContentPage contentPage in Children)
            {
                contentPage.IsVisible = false;
            }
        }
    }
}
