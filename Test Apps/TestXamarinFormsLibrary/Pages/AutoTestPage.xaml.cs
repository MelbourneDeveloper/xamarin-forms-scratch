﻿using Xamarin.Forms.Xaml;

namespace TestXamarinFormsLibrary.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AutoTestPage
    {
        public AutoTestPage()
        {
            InitializeComponent();
            ChangeSizeButton.Clicked += ChangeSizeButton_Clicked;

        }

        private void ChangeSizeButton_Clicked(object sender, System.EventArgs e)
        {
            TheEntry.HeightRequest = TheEntry.HeightRequest == 300 ? 0 : 300;
        }
    }
}