﻿using TestXamarinForms.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestXamarinForms
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AttachedPropertyPage : ContentPage
    {
        private AttachedPropertyModel Model
        {
            get
            {
                return (AttachedPropertyModel)BindingContext;
            }
            set
            {
                BindingContext = value;
            }
        }

        public AttachedPropertyPage()
        {
            InitializeComponent();

            Model = new AttachedPropertyModel { Text = "Starting Text" };

        }
    }
}
