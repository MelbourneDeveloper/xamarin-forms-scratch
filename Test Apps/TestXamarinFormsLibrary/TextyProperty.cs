﻿using Xamarin.Forms;
using System.Reflection;

namespace TestXamarinForms
{
    public static class TextyPropertyClass
    {
        private const string PropertyName = "Texty";

        public static readonly BindableProperty TextyProperty = BindableProperty.CreateAttached(PropertyName, typeof(string), typeof(BindableObject), string.Empty, propertyChanged: OnTextyChanged);

        public static bool GetTexty(BindableObject view)
        {
            return (bool)view.GetValue(TextyProperty);
        }

        public static void SetTexty(BindableObject view, string value)
        {
            view.SetValue(TextyProperty, value);
        }

        static void OnTextyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (bindable == null)
            {
                return;
            }

            var bindableType = bindable.GetType();

            var textProperty = bindableType.GetRuntimeProperty("Text");

            if (textProperty == null)
            {
                return;
            }

            textProperty.SetValue(bindable, newValue);
        }
    }
}
