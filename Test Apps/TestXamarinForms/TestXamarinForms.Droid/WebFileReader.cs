using System;
using System.Net;
using System.Threading.Tasks;

namespace TestXamarinForms
{
    public class WebFileReader : IFileReader
    {
        #region Fields
        private Uri _FileUri;
        #endregion

        #region Public Properties
        public Uri FileUri
        {
            get
            {
                return _FileUri;
            }
        }
        #endregion

        #region Constructor
        public WebFileReader(Uri fileUri)
        {
            _FileUri = fileUri;
        }
        #endregion

        #region Implementation
        public async Task<byte[]> ReadFileAsync()
        {
            var webClient = new WebClient();
            return await webClient.DownloadDataTaskAsync(_FileUri);
        }
        #endregion
    }
}